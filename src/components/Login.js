import React from 'react';

export default function Login() {
    return (
        <>
            <div className="container border col-sm-6 mx-auto bg-ligh p-3 shadow">
                    <h4 className="my-2">Login Component</h4>
                    <form>
                        <div className="form-group">
                            <label className="my-2" for="usr">Username</label>
                            <input type="email" className="form-control" id="usr" name="userName" placeholder="Enter username"/>
                        </div>
                        <div className="form-group">
                            <label className="my-2" for="pwd">Password</label>
                            <input type="password" className="form-control" id="pwd" name="password" placeholder="Enter password" />
                        </div>
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link text-danger" href="#">Forgot Password ?</a>
                            </li>
                        </ul>
                        <div className="col-sm-12 row justify-content-center">
                            <input className="btn btn-outline-success" type="submit" value="Submit" />
                        </div>
                    </form>
                </div>
        </>
    )
}